﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Splash
{
	public class GgjSplashScreen : MonoBehaviour
	{
		private void Awake()
		{
			StartCoroutine(WaitAndContinue());
		}

		private IEnumerator WaitAndContinue()
		{
			yield return new WaitForSeconds(2);

			SceneManager.LoadScene("Main");
		}
	}
}