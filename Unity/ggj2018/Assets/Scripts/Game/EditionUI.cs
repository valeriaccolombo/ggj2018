﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class EditionUI : MonoBehaviour
    {
        public GameObject SelectedPieceOptions;

        public Toggle ToggleDomino;
        public Toggle ToggleBall;
        public Toggle ToggleSelection;
    
        private void Awake()
        {
            SelectedPieceOptions.SetActive(false);
        }

        public bool IsCreationEnabled()
        {
            return !ToggleSelection.isOn;
        }

        public PieceType? PieceToCreate()
        {
            if(ToggleDomino.isOn)
                return PieceType.Domino;
            if(ToggleBall.isOn)
                return PieceType.Ball;

            return null;
        }
    }    
}
