﻿using UnityEngine;

namespace Game
{
    public enum PieceType
    {
        Domino,
        Ball
    }
    
    public class PiecesFactory
    {
        public static Piece CreatePiece(PieceType type)
        {
            var rnd = Random.Range(1, 4);
            switch (type)
            {
                case PieceType.Domino:
                    return GameObject.Instantiate(Resources.Load("Prefabs/Domino0"+rnd) as GameObject).GetComponent<Piece>();
                case PieceType.Ball:
                    return GameObject.Instantiate(Resources.Load("Prefabs/Ball0"+rnd) as GameObject).GetComponent<Piece>();
                    break;
                default:
                    return null;
            }
        }
    }
}