﻿using UnityEngine;

namespace Game
{
	public class Objetive : MonoBehaviour
	{
		private Vector3 _position;
		private Quaternion _rotation;

		public void Enable()
		{
			GetComponent<Rigidbody>().isKinematic = false;
		}

		public void Disable()
		{
			GetComponent<Rigidbody>().isKinematic = true;
		}

		private void OnCollisionEnter(Collision col)
		{
			if (col.gameObject.name != "Floor")
				GameController.Instance.Win();
		}

		public void SaveState()
		{
			if (_position != null)
			{
				_position = transform.position;
				_rotation = transform.rotation;
			}
		}

		public void RestoreState()
		{
			transform.position = _position;
			transform.rotation = _rotation;
		}
	}
}