﻿using System.Collections;
using UnityEngine;

namespace Game
{
    public class Trigger : MonoBehaviour
    {
        public float ForceX = 0;
        public float ForceY = 0;
        public float ForceZ = 0;

        private Vector3 _position;
        private Quaternion _rotation;

        private IEnumerator GoGoGo()
        {
            yield return new WaitForSeconds(0.5f);

            GetComponent<Rigidbody>().AddForce(ForceX, ForceY, ForceZ);

            StartCoroutine(Stop());
        }

        private IEnumerator Stop()
        {
            yield return new WaitForSeconds(2);

            GetComponent<Rigidbody>().velocity = Vector3.zero;
        }

        public void Go()
        {
            SaveState();
            StartCoroutine(GoGoGo());
        }

        public void Reset()
        {
            StopAllCoroutines();
            RestoreState();
        }

        private void SaveState()
        {
            if (_position != null)
            {
                _position = transform.position;
                _rotation = transform.rotation;
            }
        }

        private void RestoreState()
        {
            transform.position = _position;
            transform.rotation = _rotation;
        }
    }
}