﻿using System.Collections.Generic;
using UnityEngine;

namespace Game
{
    public class TriggersController : MonoBehaviour
    {
        public List<Trigger> Triggers;

        public void StartAll()
        {
            foreach (var trg in Triggers)
            {
                trg.Go();
            }
        }

        public void ResetAll()
        {
            foreach (var trg in Triggers)
            {
                trg.Reset();
            }
        }
    }
}