﻿using UnityEngine;
using UnityEngine.UI;

namespace Game
{
    public class ResultsUI : MonoBehaviour
    {
        public Text TextResult;
        
        public void ShowWinningResult()
        {
            TextResult.text = Locale.Instance.GetString("you_win");
            
            if(GameController.Instance.LevelNumber == 3)
                transform.Find("NextLevel").gameObject.SetActive(false);
        }
        
        public void ShowLoosingResult()
        {
            TextResult.text = Locale.Instance.GetString("you_lose");

            if(GameController.Instance.LevelNumber == 3)
                transform.Find("NextLevel").gameObject.SetActive(false);
        }
    }
}