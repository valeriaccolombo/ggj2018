﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Game
{
    public class GameController : MonoBehaviour
    {
        public static GameController Instance { get; private set; }

        public int LevelNumber = 1;
        
        public ResultsUI ResultsUI;
        public EditionUI EditionUI;
        public GameObject PlayingUI;
        public Objetive LevelObjetive;
        public TriggersController Triggers;
        public List<Piece> InitialPieces = new List<Piece>();
        
        private List<Piece> _pieces = new List<Piece>();
        private Piece _selectedPiece;
        private float _timeWithoutAction = 0;

        public bool IsPlaying { get; private set; }

        private void Awake()
        {
            Instance = this;
            
            LevelObjetive.Disable();
            EditionUI.gameObject.SetActive(true);
            ResultsUI.gameObject.SetActive(false);
            PlayingUI.SetActive(false);
            
            foreach (var piece in InitialPieces)
            {
                piece.SaveState();
            }
        }

        public void ResetAll()
        {
            foreach (var piece in _pieces)
            {
                Destroy(piece.gameObject);
            }
            _pieces.Clear();
            _selectedPiece = null;
            EditionUI.SelectedPieceOptions.SetActive(false);
        }
        
        public void OnPieceSelected(Piece selected)
        {
            if (_selectedPiece != null)
                _selectedPiece.Unselect();
            
            _selectedPiece = selected;
            EditionUI.SelectedPieceOptions.SetActive(true);
        }

        public void OnFloorClicked(Vector3 hitPoint)
        {
            if (!IsPlaying && EditionUI.IsCreationEnabled())
            {
                var piece = PiecesFactory.CreatePiece(EditionUI.PieceToCreate().Value);
                piece.transform.SetParent(transform);
                piece.transform.position = new Vector3(hitPoint.x, piece.StartingY, hitPoint.z);
        
                _pieces.Add(piece);
                piece.Select();
            }
        }

        public void RemoveSelected()
        {
            if (_selectedPiece != null)
            {
                _pieces.Remove(_selectedPiece);
                Destroy(_selectedPiece.gameObject);
                _selectedPiece = null;
                EditionUI.SelectedPieceOptions.SetActive(false);
            }
        }

        public void RotateSelected(bool direction)
        {
            if (_selectedPiece != null)
            {
                _selectedPiece.RotateY(direction ? 5 : -5);
            }
        }

        public void Play()
        {
            IsPlaying = true;

            StartCoroutine(WaitAndShowPlayingUI());
            ResultsUI.gameObject.SetActive(false);
            EditionUI.gameObject.SetActive(false);
            _timeWithoutAction = 0;
            foreach (var piece in InitialPieces)
            {
                piece.Unselect();
                piece.SaveState();
            }
            foreach (var piece in _pieces)
            {
                piece.Unselect();
                piece.SaveState();
            }
            LevelObjetive.Enable();
            Triggers.StartAll();
        }

        private IEnumerator WaitAndShowPlayingUI()
        {
            yield return new WaitForSeconds(2);
            PlayingUI.SetActive(true);
        }

        public void Edit()
        {
            StopAllCoroutines();
            IsPlaying = false;
            
            PlayingUI.SetActive(false);
            ResultsUI.gameObject.SetActive(false);
            EditionUI.gameObject.SetActive(true);
            foreach (var piece in InitialPieces)
            {
                piece.RestoreState();
            }
            foreach (var piece in _pieces)
            {
                piece.RestoreState();
            }
            LevelObjetive.Disable();
            Triggers.ResetAll();
        }

        public void GoToNextLevel()
        {
            SceneManager.LoadScene("Game"+(LevelNumber+1).ToString());
        }
        
        public void ExitToMenu()
        {
            SceneManager.LoadScene("Main");
        }

        private void Update()
        {
            if (IsPlaying)
            {
                _timeWithoutAction += Time.deltaTime;

                if (_timeWithoutAction > 7)
                {
                    Lose();
                }
            }
        }

        public void CollisionOfAnyType()
        {
            _timeWithoutAction = 0;
        }
        
        private void Lose()
        {
            if (IsPlaying)
            {
                IsPlaying = false;
                StopAllCoroutines();
                ResultsUI.gameObject.SetActive(true);
                ResultsUI.ShowLoosingResult();
            }
        }
        
        public void Win()
        {
            if (IsPlaying)
            {
                IsPlaying = false;
                StopAllCoroutines();
                ResultsUI.gameObject.SetActive(true);
                ResultsUI.ShowWinningResult();
            }
        }
    }
}