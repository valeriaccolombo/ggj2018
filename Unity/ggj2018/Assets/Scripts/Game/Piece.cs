﻿using System.Collections;
using UnityEngine;
using Utils;

namespace Game
{
    public class Piece : MonoBehaviour
    {
        public float StartingY;

        public bool Editable = true;
        public GameObject Selection;

        private Vector3 _position;
        private Quaternion _rotation;

        private void Awake()
        {
            Selection.SetActive(false);
        }

        public void OnMouseDown()
        {
            if (!TouchUtils.IsTouchingUI())
                Select();
        }

        public void Unselect()
        {
            Selection.SetActive(false);
        }

        public void Select()
        {
            if (Editable && !GameController.Instance.IsPlaying)
            {
                GameController.Instance.OnPieceSelected(this);
                Selection.SetActive(true);
            }
        }

        public void RotateY(float speed)
        {
            transform.Rotate(0, speed, 0);
        }

        private void OnCollisionEnter()
        {
            GameController.Instance.CollisionOfAnyType();
        }

        public void SaveState()
        {
            if (!Editable && _position != null)
                return;
            
            _position = transform.position;
            _rotation = transform.rotation;
        }

        public void RestoreState()
        {
            transform.position = _position;
            transform.rotation = _rotation;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            StartCoroutine(ISaidStop(true));
        }

        private IEnumerator ISaidStop(bool retry)
        {
            yield return new WaitForSeconds(0.25f);

            GetComponent<Rigidbody>().velocity = Vector3.zero;
            transform.position = _position;
            transform.rotation = _rotation;
            GetComponent<Rigidbody>().velocity = Vector3.zero;
            
            if(retry)
                StartCoroutine(ISaidStop(false));
        }
    }
}