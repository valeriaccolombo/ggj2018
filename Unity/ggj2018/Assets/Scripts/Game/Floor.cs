﻿using Game;
using UnityEngine;
using Utils;

namespace Game
{
	public class Floor : MonoBehaviour 
	{
		public void OnMouseDown()
		{
			if (!TouchUtils.IsTouchingUI())
			{
				RaycastHit hit;
				var ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				if (Physics.Raycast (ray, out hit, 100.0f))
				{
					if (hit.collider.gameObject == this.gameObject)
					{
						GameController.Instance.OnFloorClicked(hit.point);
					}
				}
			}
		}
	}	
}
