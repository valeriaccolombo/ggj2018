﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LangSelection : MonoBehaviour
{
	public List<Localization> SceneTexts;
	public Toggle ToggleEn;
	public Toggle ToggleEs;
	public Toggle TogglePt;
	
	public void OnLangChanged()
	{
		if(ToggleEs.isOn)
			Locale.Instance.CurrLang = "es";
		else if(TogglePt.isOn)
			Locale.Instance.CurrLang = "pt";
		else
			Locale.Instance.CurrLang = "en";
		
		ReloadAllSceneTexts();
	}

	private void ReloadAllSceneTexts()
	{
		foreach (var localization in SceneTexts)
		{
			localization.Reload();
		}
	}
}
