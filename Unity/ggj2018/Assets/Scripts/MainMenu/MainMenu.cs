﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace MAinMenu
{
	public class MainMenu : MonoBehaviour
	{
		public GameObject CreditsScreen;

		private void Awake()
		{
			CreditsScreen.SetActive(false);
		}

		public void OnStartButtonClick()
		{
		    SceneManager.LoadScene("Game1");
		}

		public void OnExitGameButtonClick()
		{
			Application.Quit();
		}
		
		public void OnCreditsButtonClick()
		{
			CreditsScreen.SetActive(true);
		}

		public void OnCreditsScreenTap()
		{
			CreditsScreen.SetActive(false);
		}
	}
}