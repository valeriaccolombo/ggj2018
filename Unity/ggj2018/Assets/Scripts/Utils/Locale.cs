﻿using System.Collections.Generic;

public class Locale
{
    private static Locale _instance;
    public static Locale Instance
    {
        get
        {
            if(_instance == null)
                _instance = new Locale();

            return _instance;
        }
    }

    public string CurrLang = "en";
    private Dictionary<string, string> _tids;
    
    public Locale()
    {
        CurrLang = "en";
    }
    
    private void Initialize()
    {
        _tids = new Dictionary<string, string>();

        _tids["start_game_en"] = "Start";
        _tids["start_game_es"] = "Jugar";
        _tids["start_game_pt"] = "Começar";
        
        _tids["credits_en"] = "Credits";
        _tids["credits_es"] = "Créditos";
        _tids["credits_pt"] = "Créditos";
        
        _tids["exit_en"] = "Exit";
        _tids["exit_es"] = "Salir";
        _tids["exit_pt"] = "Sair";
        
        _tids["credits_roles_en"] = "Game Design and Programming";
        _tids["credits_roles_es"] = "Game Design y Programación";
        _tids["credits_roles_pt"] = "Design de jogo e programação";
        
        _tids["credits_music_en"] = "Music By Lobo Loco - Loners in the Night";
        _tids["credits_music_es"] = "Música por Lobo Loco - Loners in the Night";
        _tids["credits_music_pt"] = "Música por Lobo Loco - Loners in the Night";
             
        _tids["reset_all_en"] = "Reset All";
        _tids["reset_all_es"] = "Borrar Todo";
        _tids["reset_all_pt"] = "Reiniciar";
        
        _tids["edition_en"] = "Edition";
        _tids["edition_es"] = "Editar";
        _tids["edition_pt"] = "Editar";
        
        _tids["ball_en"] = "+ Ball";
        _tids["ball_es"] = "+ Canica";
        _tids["ball_pt"] = "+ Mármore";
        
        _tids["domino_en"] = "+ Domino";
        _tids["domino_es"] = "+ Ficha";
        _tids["domino_pt"] = "+ Dominó";
        
        _tids["play_en"] = "Play";
        _tids["play_es"] = "Jugar";
        _tids["play_pt"] = "Jogar";
        
        _tids["try_again_en"] = "Try Again";
        _tids["try_again_es"] = "Reintentar";
        _tids["try_again_pt"] = "Reintentar";
     
        _tids["next_level_en"] = "Next Level";
        _tids["next_level_es"] = "Siguiente Nivel";
        _tids["next_level_pt"] = "Próximo Nível";
        
        _tids["exit_menu_en"] = "Exit to Menu";
        _tids["exit_menu_es"] = "Salir al Menu";
        _tids["exit_menu_pt"] = "Saia para o Menu";
        
        _tids["you_win_en"] = "YOU WIN!!!";
        _tids["you_win_es"] = "¡GANASTE!";
        _tids["you_win_pt"] = "VOCÊ GANHA!!!";
        
        _tids["you_lose_en"] = "YOU LOSE!!!";
        _tids["you_lose_es"] = "¡PERDISTE!";
        _tids["you_lose_pt"] = "VOCÊ PERDEU!!!";    
    }
    
    public string GetString(string key)
    {
        if(_tids == null)
            Initialize();

        if (_tids.ContainsKey(key+"_"+CurrLang))
            return _tids[key+"_"+CurrLang];
        else
            return "KEY_NOT_FOUND";
    }
}
