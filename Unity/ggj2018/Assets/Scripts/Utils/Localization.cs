﻿using UnityEngine;
using UnityEngine.UI;

public class Localization : MonoBehaviour
{
    public string LocaleKey;
    public bool IsUpperCase = false;
    
    private Text _text;
    
    private void Start()
    {
        _text = GetComponent<Text>();
        
        if (IsUpperCase)
            _text.text = Locale.Instance.GetString(LocaleKey).ToUpper();
        else
            _text.text = Locale.Instance.GetString(LocaleKey);
    }

    public void Reload()
    {
        if (_text != null)
        {
            if (IsUpperCase)
                _text.text = Locale.Instance.GetString(LocaleKey).ToUpper();
            else
                _text.text = Locale.Instance.GetString(LocaleKey);
        }
    }
}
