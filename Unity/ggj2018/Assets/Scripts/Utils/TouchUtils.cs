﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Utils
{
	public class TouchUtils
	{
		public static bool IsTouchingUI()
		{
			if(Input.touchCount > 0)
			{
				return EventSystem.current.IsPointerOverGameObject(Input.GetTouch (0).fingerId);
			}
			return EventSystem.current.IsPointerOverGameObject(); //<--mouse
		}
	}
}